import { TestAction } from "../actions";

export const initialState: TestReducerState = {

};

export interface TestReducerState {

};

export type TestReducerActions = TestAction;

const testReducer = (state = initialState, action: TestReducerActions): TestReducerState => {
    switch(action.type) {
        default:
            return state;
    }
};

export default testReducer;
