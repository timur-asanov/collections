import { takeLatest } from 'redux-saga/effects';
import { TestAction } from "../actions";

export function* testingAction(action: TestAction) {
    try {
        console.log(action);
    } catch (e) {
        console.log(e);
    }
}

export function* watchTestSaga(): any {
    yield takeLatest("TEST_ACTION", testingAction);
}

export default watchTestSaga;
