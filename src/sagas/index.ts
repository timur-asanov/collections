import { all } from 'redux-saga/effects';
import watchTestSaga from "./testSaga";

export default function* sagas(): any {
    yield all([
        watchTestSaga()
    ]);
};
