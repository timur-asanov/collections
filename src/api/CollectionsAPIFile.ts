import { CollectionsAPI } from "./APIInterfaces";
import ItemCountsResponse from "../model/api/ItemCountsResponse";
import { ComicBookItem } from "../model/ComicBookItem";
import comicBooksJSON from "./test_data/comics.json";

class CollectionsAPIFile implements CollectionsAPI {

	public constructor() {
		console.log("created CollectionsAPIFile");
	}

	public getItemCounts(): ItemCountsResponse {
		console.log("fetching item counts");

		let itemCountsResponse: ItemCountsResponse = new ItemCountsResponse();

		if (comicBooksJSON && comicBooksJSON.length > 0) {
			const comicBooks: Array<ComicBookItem> = Object.assign([], comicBooksJSON);

			itemCountsResponse.uniqueComics = comicBooks.length;

			itemCountsResponse.comics = comicBooks.reduce((accum: number, comicBook: ComicBookItem) => {
				return accum += comicBook.ownedNumber;
			}, 0);
		}

		return itemCountsResponse;
	}
}

export default CollectionsAPIFile;