import { CollectedItemAPI } from "./APIInterfaces";
import { ComicBookItem } from "../model/ComicBookItem";
import comicBooksJSON from "./test_data/comics.json";

export class CollectedItemAPIFileComics implements CollectedItemAPI<ComicBookItem> {

	public constructor() {
		console.log("created CollectedItemAPIFileComics");
	}

	public getItems(pageNumber: number, pageSize: number): Array<ComicBookItem> | undefined {
		console.log("fetching comics page: " + pageNumber + " with page size: " + pageSize);

		if (comicBooksJSON && comicBooksJSON.length > 0) {
			const comicBooks: Array<ComicBookItem> = Object.assign([], comicBooksJSON);

			return comicBooks.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
		}

		return undefined;
	}

	public getItem(id: string): ComicBookItem | undefined {
		console.log("fetching comic: " + id);

		if (comicBooksJSON && comicBooksJSON.length > 0) {
			const comicBooks: Array<ComicBookItem> = Object.assign([], comicBooksJSON);

			return comicBooks.find(comicBook => comicBook.id === id);
		}

		return undefined;
	}

	public getCount(): number {
		if (comicBooksJSON && comicBooksJSON.length > 0) {
			const comicBooks: Array<ComicBookItem> = Object.assign([], comicBooksJSON);

			return comicBooks.reduce((accum: number, comicBook: ComicBookItem) => {
				return accum += comicBook.ownedNumber;
			}, 0);
		}

		return 0;
	}

	public getUniqueCount(): number {
		if (comicBooksJSON) {
			return comicBooksJSON.length;
		}

		return 0;
	}
}

export default CollectedItemAPIFileComics;