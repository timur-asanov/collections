import CollectionsAPIFile from "./CollectionsAPIFile";
import { CollectionsAPI } from "./APIInterfaces";

export class APIClientCollections {

	private static _instance: CollectionsAPI;

	private constructor() {}

	public static get instance(): CollectionsAPI {
		return this._instance || this.createInstance();
	}

	private static createInstance(): CollectionsAPI {
		// TODO type of instance should be configurable
		this._instance = new CollectionsAPIFile();

		return this._instance;
	}
}