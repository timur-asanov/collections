import ItemCountsResponse from "../model/api/ItemCountsResponse";

export interface CollectionsAPI {
	getItemCounts: () => ItemCountsResponse;
}

export interface CollectedItemAPI<Type> {
	getItems: (pageNumber: number, pageSize: number) => Array<Type> | undefined;
	getItem: (id: string) => Type | undefined;
	getCount: () => number;
	getUniqueCount: () => number;
}