import { CollectedItemAPI } from "./APIInterfaces";
import { ComicBookItem } from "../model/ComicBookItem";
import { CollectedItemAPIFileComics } from "./CollectedItemAPIFileComics";

export class CollectedItemAPIComics {

	private static _instance: CollectedItemAPI<ComicBookItem>;

	public static get instance(): CollectedItemAPI<ComicBookItem> {
		return this._instance || this.createInstance();
	}

	private static createInstance(): CollectedItemAPI<ComicBookItem> {
		// TODO type of instance should be configurable
		this._instance = new CollectedItemAPIFileComics();

		return this._instance;
	}

	private constructor() {}
}

export default CollectedItemAPIComics;