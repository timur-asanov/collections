import { CollectedItem, CollectedItemType } from "./CollectedItem";

export class ComicBookItem extends CollectedItem {
	private _series: string;
	private _publisher: string;
	private _issueNumber: string;
	private _publishedDate: string;
	private _ownedNumber: number;

    constructor(id: string, series: string, publisher: string, issueNumber: string, publishedDate: string, ownedNumber: number) {
    	// TODO series or issueNumber
        super(id, series, CollectedItemType.COMIC_BOOK);

        this._series = series;
        this._publisher = publisher;
        this._issueNumber = issueNumber;
        this._publishedDate = publishedDate;
        this._ownedNumber = ownedNumber;
    }

	get series(): string {
        return this._series;
    }

    set series(series: string) {
        this._series = series;
    }

	get publisher(): string {
        return this._publisher;
    }

    set publisher(publisher: string) {
        this._publisher = publisher;
    }

	get issueNumber(): string {
        return this._issueNumber;
    }

    set issueNumber(issueNumber: string) {
        this._issueNumber = issueNumber;
    }

	get publishedDate(): string {
        return this._publishedDate;
    }

    set publishedDate(publishedDate: string) {
        this._publishedDate = publishedDate;
    }

	get ownedNumber(): number {
        return this._ownedNumber;
    }

    set ownedNumber(ownedNumber: number) {
        this._ownedNumber = ownedNumber;
    }
}
