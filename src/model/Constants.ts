import { TableColumn } from "@npmtima-org/simple-table";

export const DEFAULT_COMICS_TABLE_COLUMNS = (): Array<TableColumn> => {
	const columns: Array<TableColumn> = [];
	columns.push(new TableColumn({columnKey: "id", headerText: "ID"}));
	columns.push(new TableColumn({columnKey: "series", headerText: "Series"}));
	columns.push(new TableColumn({columnKey: "publisher", headerText: "Publisher"}));
	columns.push(new TableColumn({columnKey: "issueNumber", headerText: "Issue Number", alignRight: true}));
	columns.push(new TableColumn({columnKey: "publishedDate", headerText: "Published Date", alignRight: true}));
	columns.push(new TableColumn({columnKey: "ownedNumber", headerText: "# Owned", hidden: true}));

	return columns;
};
