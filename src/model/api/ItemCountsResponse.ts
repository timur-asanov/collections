class ItemCountsResponse {
	private _comics: number = 0;
	private _uniqueComics: number = 0;

	get comics(): number {
        return this._comics;
    }

    set comics(comics: number) {
        this._comics = comics;
    }

	get uniqueComics(): number {
        return this._uniqueComics;
    }

    set uniqueComics(uniqueComics: number) {
        this._uniqueComics = uniqueComics;
    }
}

export default ItemCountsResponse;