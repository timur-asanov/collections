interface CollectedItemInterface {

}

export enum CollectedItemType {
    COMIC_BOOK = "COMIC_BOOK"
}

export class CollectedItem implements CollectedItemInterface {
    private _id: string;
    private _name: string;
    private _type: CollectedItemType;

    constructor(id: string, name: string, type: CollectedItemType) {
        this._id = id;
        this._name = name;
        this._type = type;
    }

    get id(): string {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get type(): CollectedItemType {
        return this._type;
    }
}
