import React from "react";
import Table, { TableColumn, ExtendedRowData, DEFAULT_PAGE_SIZE } from "@npmtima-org/simple-table";
import { ComicBookItem } from "../model/ComicBookItem";
import { CollectedItemAPIComics } from "../api/CollectedItemAPIComics";
import { DEFAULT_COMICS_TABLE_COLUMNS } from "../model/Constants";

interface ComicsPropsInterface {}

interface ComicsStateInterface {
	totalComics: number;
	comicsList: Array<ComicBookItem> | undefined;
	loadingList: boolean;
	tableData: Array<any>;
	tableColumns: Array<TableColumn>;
}

class Comics extends React.Component<ComicsPropsInterface, ComicsStateInterface> {
	constructor(props : ComicsPropsInterface) {
		super(props);

		this.state = {
			totalComics: 0,
			comicsList: undefined,
			loadingList: false,
			tableData: [],
			tableColumns: [...DEFAULT_COMICS_TABLE_COLUMNS()]
		};
	}

	componentDidMount() {
		this.loadComicsPage(1, DEFAULT_PAGE_SIZE);
	}

	loadComicsPage = (pageNumber: number, pageSize: number) => {
		this.setState({
			loadingList: true
		});

		const totalComics: number = CollectedItemAPIComics.instance.getUniqueCount();

		const newComicsList: Array<ComicBookItem> | undefined = CollectedItemAPIComics.instance.getItems(pageNumber, pageSize);
		let newTableData: Array<any> = [];

		if (newComicsList && newComicsList.length > 0) {
			newTableData = newComicsList.map(comicItem => {
				return {
					id: comicItem.id,
					data: {
						series: comicItem.series,
						publisher: comicItem.publisher,
						issueNumber: comicItem.issueNumber,
						publishedDate: comicItem.publishedDate,
						ownedNumber: comicItem.ownedNumber
					},
					rowDetails: new ExtendedRowData({headerText: comicItem.id, description: undefined})
				};
			});
		}

		this.setState({
			totalComics: totalComics,
			comicsList: newComicsList,
			tableData: [...newTableData],
			loadingList: false
		});
	}

	onPageChange = (pageNumber: number, pageSize: number): void => {
		this.loadComicsPage(pageNumber, pageSize);
	}

    render() {
        return (
            <div>
                <Table 
                	columns={this.state.tableColumns} 
                	tableData={this.state.tableData} 
                	totalRows={this.state.totalComics} 
                	onPageChange={this.onPageChange} 
                	expandableRows={true} 
                	loading={this.state.loadingList} 
                />
            </div>
        );
    }
}

export default Comics;
