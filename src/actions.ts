export const TEST_ACTION = "TEST_ACTION";

export interface TestAction {
    type: typeof TEST_ACTION;
};

export const sendTestAction = ():TestAction  => {
    return {
        type: TEST_ACTION
    };
};
