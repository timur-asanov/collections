import React, { useState, MouseEvent, MouseEventHandler } from 'react';
import { HashRouter, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { sendTestAction, TestAction } from "./actions";
// import { APIClientCollections } from "./api/CollectionsAPI";
// import ItemCountsResponse from "./model/api/ItemCountsResponse";
import Header from "./components/header/Header";
import Home from "./views/Home";
import Comics from "./views/Comics";
import styles from "./App.module.css";

interface AppInput {
    sendTestAction: () => TestAction;
}

const App = ({sendTestAction}: AppInput) => {
    const [isMenuOpen, setMenuOpen] = useState<boolean>(false);

    // const testAnAction = () => {
    //     console.log("sending test action..");
    //     sendTestAction();

    //     const itemCounts: ItemCountsResponse = APIClientCollections.instance.getItemCounts();
    //     console.log(itemCounts);
    // };

    const toggleMenu = (event: MouseEvent<HTMLButtonElement>): void => {
        event.preventDefault();
        setMenuOpen(!isMenuOpen);
    };

    const handleNavigation = (): MouseEventHandler<HTMLAnchorElement> | undefined =>  {
        setMenuOpen(false);

        return undefined;
    };

    // <button onClick={testAnAction}>Send Test Action</button>

    return (
        <HashRouter basename="/">
            <Header isMenuOpen={isMenuOpen} onMenuToggle={toggleMenu} navigationHandler={handleNavigation} />

            <div className={styles.contentWrapper}>
                <div className="grid">
                    <div className="col-1-1">
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/comics" component={Comics} />
                            <Route component={Home} />
                        </Switch>
                    </div>
                </div>
            </div>
        </HashRouter>
    );
};

const mapStateToProps = (state: any, ownProps: any) => {
    return {};
};

const mapDispatchToProps = { sendTestAction };

export default connect(mapStateToProps, mapDispatchToProps)(App);
