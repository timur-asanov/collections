import React, { MouseEvent, MouseEventHandler } from 'react';
import Menu from "./menu/Menu";
import styles from './Header.module.css';

interface HeaderInput {
    isMenuOpen: boolean;
    onMenuToggle: (event: MouseEvent<HTMLButtonElement>) => void;
    navigationHandler: () => MouseEventHandler<HTMLAnchorElement> | undefined;
}

const Header = ({ isMenuOpen, onMenuToggle, navigationHandler }: HeaderInput) => {
    return (
        <div className={styles.root}>
        	<header className={(isMenuOpen === true ? styles.isOpen : '')}>
        		<Menu isMenuOpen={isMenuOpen} onMenuToggle={onMenuToggle} navigationHandler={navigationHandler} />
        	</header>
        </div>
    );
};

export default Header;
