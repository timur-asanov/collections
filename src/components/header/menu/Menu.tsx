import React, { MouseEvent, MouseEventHandler } from 'react';
import { NavLink } from "react-router-dom";
import styles from './Menu.module.css';

interface MenuInput {
    isMenuOpen: boolean;
    onMenuToggle: (event: MouseEvent<HTMLButtonElement>) => void;
    navigationHandler: () => MouseEventHandler<HTMLAnchorElement> | undefined;
}

const Menu = ({ isMenuOpen, onMenuToggle, navigationHandler }: MenuInput) => {
    return (
        <div className={styles.root}>
            <button onClick={onMenuToggle} className={styles.hamburger + ' ' + (isMenuOpen === true ? styles.isOpen : '')}>
                <span className={styles.hamburgerBox}>
                    <span className={styles.hamburgerInner}></span>
                </span>
            </button>

            <nav className={(isMenuOpen === true ? styles.isOpen : '')}>
                <div className={styles.menuColumn}>
                    <span className={styles.menuColumnHeader}>Reading</span>
                    <ul>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Books</NavLink></li>
                        <li><NavLink exact to="/comics" onClick={navigationHandler}>Comic Books</NavLink></li>
                    </ul>
                </div>

                <div className={styles.menuColumn}>
                    <span className={styles.menuColumnHeader}>Entertainment</span>
                    <ul>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Video Games</NavLink></li>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Movies</NavLink></li>
                        <li><NavLink exact to="/" onClick={navigationHandler}>TV Shows</NavLink></li>
                    </ul>
                </div>

                <div className={styles.menuColumn}>
                    <span className={styles.menuColumnHeader}>Toys</span>
                    <ul>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Funko Pop</NavLink></li>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Other</NavLink></li>
                    </ul>
                </div>

                <div className={styles.menuColumn}>
                    <span className={styles.menuColumnHeader}>Cards</span>
                    <ul>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Playing Cards</NavLink></li>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Pokemon Cards</NavLink></li>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Sports Cards</NavLink></li>
                    </ul>
                </div>

                <div className={styles.menuColumn}>
                    <span className={styles.menuColumnHeader}>Miscellaneous</span>
                    <ul>
                        <li><NavLink exact to="/" onClick={navigationHandler}>Music</NavLink></li>
                    </ul>
                </div>
            </nav>
        </div>
    );
};

export default Menu;
