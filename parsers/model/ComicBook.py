import uuid

class ComicBook(object):
    def __init__(self, series = None, publisher = None, issueNumber = None, publishedDate = None, ownedNumber = None, *args, **kwargs):
        self.id = str(uuid.uuid4())
        self.series = series
        self.publisher = publisher
        self.issueNumber = issueNumber
        self.publishedDate = publishedDate
        self.ownedNumber = ownedNumber

    def __repr__(self):
        return "<ComicBook id:{} series:{} publisher:{} issueNumber:{} publishedDate:{} ownedNumber:{}>".format(self.id, self.series, self.publisher, self.issueNumber, self.publishedDate, self.ownedNumber)

    def __str__(self):
        return repr(self)
