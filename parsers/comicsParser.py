import os
import re
import json
import csv
from pathlib import Path

from model.ComicBook import ComicBook

DATA_FILE = Path("{}/data/comics_stash_202104.csv".format(os.getcwd()))
DESTINATION_FILE = Path("{}/src/api/test_data/comics.json".format(os.pardir))

def write_to_destination(comics):
    json_all = json.dumps(comics, default=serialize)

    output_file = open(DESTINATION_FILE, "w")
    output_file.write(json_all)
    output_file.close()

def serialize(obj):
    return obj.__dict__

def parse_data_file():
    comics = []

    if DATA_FILE.is_file():
    	with open(DATA_FILE, newline='') as csv_file:
    		csv_reader = csv.DictReader(csv_file)
    		total_comics = 0

    		for row in csv_reader:
    			comic = ComicBook(row["Comic Title"], row["Publisher"], row["Issue #"], row["Published Date"], int(row["# of Issues I Own"]))

    			if comic is not None:
    				total_comics += comic.ownedNumber
    				comics.append(comic)

    		print(f'Total comics: {total_comics}.')

    	csv_file.close()

    return comics

def main():
    try:
        print("data file: {}".format(DATA_FILE))
        print("destination file: {}".format(DESTINATION_FILE))

        comics = parse_data_file()

        print(f'Unique comics: {len(comics)}.')

        if comics is not None and len(comics) > 0:
            write_to_destination(comics)
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()
